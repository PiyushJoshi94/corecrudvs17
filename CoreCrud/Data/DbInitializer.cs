﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreCrud.Model;

namespace CoreCrud.Data
{
    public static class DbInitializer
    {
       public static  void Initialize(EmployeeContext employeeContext)
        {
            employeeContext.Database.EnsureCreated();

            if (employeeContext.Employee.Any())
            {
                return;   // DB has been seeded
            }

            var employees = new Employee[] {
                new Employee {EmployeeFirstName="amit",EmployeeLastName="bisht"},
                new Employee {EmployeeFirstName="nicklesh",EmployeeLastName="bisht"}
            };
            
            foreach(var employee in employees)
            {
                employeeContext.Employee.Add(employee);
              
            }

            employeeContext.SaveChanges();

        }
    }
}
