﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreCrud.Model
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }

        [Display(Name ="First Name")]
        [Required(ErrorMessage ="First Name is Required")]
        public string EmployeeFirstName { get; set; }

        [Display(Name ="Last Name")]
        [Required(ErrorMessage = "Last Name is Required")]
        public string EmployeeLastName { get; set; }
    }
}
